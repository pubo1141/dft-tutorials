import ase.eos as ase_eos
import ase.units as ase_units

def read_datafile(filename,factor):

    volumes = []
    energies = []
    with open(filename, 'r') as f:
        for line in f:
            if line.strip() != '':
                volume = (float(line.split()[0])*ase_units.Bohr)**3.0/factor
                energy = float(line.split()[1])*ase_units.Rydberg
                volumes.append(volume)
                energies.append(energy)
    return volumes, energies

def main():
    # Take lattice type from user input:
    lattice_type = input('Enter lattice type (fcc or bcc): ')
    if lattice_type != 'fcc' and lattice_type != 'bcc':
        print('Unknown lattice type')
    else:

        if lattice_type == 'fcc':
            factor = 4.0
        else:
            factor = 2.0
        # read the data-file
        volumes, energies = read_datafile('etot_vs_alat.dat', factor)

        # Fit to the Birch-Murnaghan Equation
        eos = ase_eos.EquationOfState(volumes, energies, eos='birchmurnaghan')
        v0, e0, B = eos.fit()

        print('a_0', round((factor*v0)**0.333/ase_units.Bohr,4), 'Bohr')
        print('V_0', round(v0,4), 'AA^3')
        print('B_0', round(B / ase_units.kJ * 1.0e24,4), 'GPa')

if __name__ == '__main__':
    main()