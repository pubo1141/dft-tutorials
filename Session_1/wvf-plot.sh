#! /bin/bash

ml hpc-env/8.3
ml QuantumESPRESSO/6.7-iomkl-2019b


PP='mpirun -np 1 pp.x' 

cat >input_pp <<EOF
&INPUTPP
prefix = "SCF"
outdir = "output"
plot_num = 7
filplot = "si.band.tmp"
kpoint = 1 
kband = 4 
/
&PLOT
 nfile =1 
 filepp(1) = "si.band.tmp"
 iflag =3 
 output_format = 5
 fileout="si.band-4.kpt-1.wvf"
/
EOF

${PP} <input_pp

