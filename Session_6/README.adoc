:stem: latexmath

= xc functionals -- LDA and GGA

In this session two approximation will take in account to discribe the exchange-correlation latexmath:[E_{XC}] energy. This is essencial to apply since this functional is unknown. 

** Local density approximations (LDA) 

The local density approximation assumes that the exchange-correlation potential υxc depends only on the electronic density and it is exact for an homogeneous electron gas (HEG). 
[stem]

++++
E_{XC}^{LDA}= \int n(\textbf{r}) {\epsilon}_{\text{xc}}^{HEG} n(\textbf{r})d\textbf{r}
++++

** Generalized gradient approximations (GGA)

The idea to include gradients of the density in the exchange-correlation potential came from the early realization that LDA is not a good approximation for systems where the density varies rapidly. Including the gradient by considering a Taylor expansion of latexmath:[E_{XC}] in terms of the density, would take into account some non-locality and therefore larger variations of the electron gas density. It was found that more general functions of latexmath:[n(\textbf{r})] and latexmath:[\nabla n(\textbf{r})] (instead of a power-series) work much more satisfactorily. These approximations are called the generalized gradient approximations (GGA). 
[stem]

++++
E_{XC}^{GGA}= \int n(\textbf{r}) {\epsilon}_{\text{xc}}^{GGA} (n(\textbf{r}), \nabla n(\textbf{r})) d\textbf{r}
++++



= Different pseudopotential

In previous sessions the exchange-correlation energy was taking into account in the calculation by LDA, this information is in the specific pseudopotential, which defines the type of pseudopotential, employing to solve the Kohn-Sham equation. 

The GGA pseudopotential employs in this section is base on the PBE functional, which is the one developed by Perdew, Burke, and Ernzerhof (PBE).

= Tasks

** Study the effect of the LDA and GGA functionals on the electronic properties of Si and Al. To do so, use the Si and Al input files to solve the Khon-Sham equation and calculate the electronic band, as well as the density of state.
