= DFT Tutorials
Author Holger Saßnick

The goal of the following tutorial sessions is to apply the newly gained theoretical knowledge from the lecture "Density-functional theory" on bulk and 2D model systems.
Throughout the sessions the open-source software package Quantum Espresso will be used. It can be downloaded link:https://www.quantum-espresso.org/[here].
To visualize the input- and output-data we will use the software-package XCrySDen, also freely available link:http://www.xcrysden.org/[here].


== Session 1: Set up, Preliminaries and Visualization

In session 1 we will elaborate on how to access the high-performance cluster CARL of the Universität Oldenburg. A second task will be the visualization of crystal unit cells, Brillouin zones and Fermi surfaces.

link:../Session_1/[Link to Session 1]

== Session 2: SCF calculations: basic convergence tests (k-mesh and pw-cutoffs)

We will start off in session 2 by solving for the first time the Kohn-Sham equation in a self-consistent iterative manner.
Subject of the calculations are two stereotypical materials representing the materials class of semiconductors and metals, namely silicon and iron.
We will discuss the implemented basis set and the numerical parameters that are needed to obtain converged and reliable results.

link:../Session_2/[Link to Session 2]

== Session 3: Structure Optimization with the Birch-Murnaghan Equation of State

In this session we will derive the optimized lattice parameters of the two materials iteratively by calculating the volume that minimizes the energy with the Birch-Murnaghan equation.
As a byproduct we obtain the bulk moduli of the two crystals.

link:../Session_3/[Link to Session 3]

== Session 4: Band Structures and (P)DOS

Now we turn to the electronic structure of the two materials.
We will calculate the band structure along a specified path in the Brillouin zone and integrate over the k-points to obtain the density of states (DOS) and projected density of states (PDOS).

link:../Session_4/[Link to Session 4]

== Session 5: Spin-polarized DFT

So far we have assumed that the orbitals or Eigenstates of the Hamiltonian are occupied each by two electrons; one with spin-up and one with spin-down.
In most cases this approximation is well suited. However, in some materials the degeneracy of spin-up and spin-down orbitals does not hold.

To remedy this problem we will perform spin-unrestricted calculations, meaning that two coupled systems of Kohn-Sham equations are solved; one for spin-up and one for spin-down electrons.

link:../Session_5/[Link to Session 5]

== Session 6: xc functionals -- LDA and GGA

Up to this point we have used the local density approximation to calculate the exchange-correlation functional.
This approximation has been proven very suitable for metals, however, many correlation-effects and not covered.
In this session we will compare the LDA with the PBE parametrization of the general gradient approximation.

link:../Session_6/[Link to Session 6]

== Session 7: xc functionals -- hybrids & SCAN

Now we go even further and consider some more advanced and state-of-art functionals.
The SCAN functional belongs to the group of meta-General Gradient Approximations (meta-GGA) which include the second derivative of the electron density.
Hybrid functionals include a fraction of the exact exchange term.

link:../Session_7/[Link to Session 7]

== Session 8: low-dimensional materials

In this section, low-dimensional materials will be studied. Starting with graphene and then continuing with C3N and C3B.
From zero-gap material to semiconductor: doped material.
Effect of the hybrid functional (and SCAN) on the electronic properties of the three materials.

link:../Session_8/[Link to Session 8]

== Session 9: ???

